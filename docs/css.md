---
title: css
slug: css
---
## Propriétés css

### sélecteurs css
La mise en forme css se définit par un ensemble de règles, composées de déclarations, appliquées aux balises html. La première partie d’une règle, celle qui indique l’élément ou les éléments html à laquelle elle s’applique, soit la partie `div` dans la règle:  

``` css
div{
   background:red;
}
```  
correspond à ce que l’on appelle le sélecteur. Sous sa forme la plus simple, un sélecteur désigne une balise. La règle ci-dessus indique que, dans la page html, tous les éléments dont la balise est `<div>` auront un fond de couleur rouge.  


### typographie

[Lien utilisé !](http://zone47.com/xhtml/typographie.php)

Quelques exemples (et il y en a plein d'autres !!):  
`font-family: 'Times New Roman', Times, serif;`  changement de fonte.  
`font-size: 12pt;` changement du corps (px, pt, em,  rem, vw)   
`color: #ff0000;` changement  de la couleur de la fonte.  
`font-style: italic;` changement du style de fonte (normal, oblique ...)  
`font-weight: bold;` changement de graisse (bold, lighter, 100, 200, 300 ...)  
`font-variant: small-caps slashed-zero;` changement de case typographique
`text-decoration: underline; ` souligné, surligné le text.  
`line-height: 14px;` changement d'interlignage (px, pt, em ...)   
`text-align: center;` changement de justification du texte (center, left, right, justify)  

Pour importer une autre fonte qu'une fonte «système» nous devons faire un `@font-face`.
À déclarer en haut du document css.

```
@font-face {
	font-family: "nomDeLafont";
	src: url("chemin/vers/mon/fichier/fonte.otf");
}
```  
Pour l'appliquer sur un bloc il suffit de faire :  

```
p {
	font-family: "nomDeLafont";
}
```  

### blocs  

Quelques exemples (et il y en a plein d'autres !!):  
`position: absolute;`  définir le comportement de l'emplacement du bloc (fixed, relative, static, sticky).  
`width: 200px;` définir la largeur du bloc (px, pt, mm, cm, em, %)  
`height: 200px;` définir la hauteur du bloc (px, pt, mm, cm, em, %)  
`top: 200px;` définir la position du bloc à partir du haut de la page  
`bottom: 200px;` définir la position du bloc à partir du bas de la page  
`left: 200px;` définir la position du bloc à partir du coté gauche de la page  
`right: 200px;` définir la position du bloc à partir du coté droit de la page  
`background-color: red;` définir la couleur de fond du bloc  
`background-image: url('http://exemple.net/monimage.jpg');` définir une image de fond du bloc  
`margin: 20px;` définir une marge de 20px tout autour du bloc  
`margin-top: 20px;` définir une marge de 20px au dessus du bloc  
`margin-bottom: 20px;` définir une marge de 20px en dessous du bloc  
`margin-right: 20px;` définir une marge de 20px à droite du bloc  
`margin-left: 20px;` définir une marge de 20px à gauche du bloc  
`padding: 20px;` définir une marge de 20px tout autour du bloc  
`padding-top: 20px;` définir une marge intérieure supérieur de 20px  
`padding-bottom: 20px;` définir une marge intérieure inférieur de 20px  
`padding-right: 20px;` définir une marge intérieure droite de 20px  
`padding-left: 20px;` définir une marge intérieure gauche de 20px  
`column-count: 2;` définir deux colonnes au bloc  
`column-gap: 10pt` définir une goutière de 10pt entre les colonnes 

<small>
	**Utilisation avancée des blocs**  
	Le css grid -> [https://css-tricks.com/snippets/css/complete-guide-grid/](https://css-tricks.com/snippets/css/complete-guide-grid/)  
	Les FlexBox -> [https://css-tricks.com/snippets/css/a-guide-to-flexbox/](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
	
</small>
