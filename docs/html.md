---
title: HTML
slug: html
---
### Structure de page par défaut

Voilà notre structure de base de notre page de visualisation.

```html
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>mon édition</title>
		<link rel="stylesheet" href="style.css" />
	</head>
	<body>

		votre texte

	</body>
	<script src="main.js" ></script>
</html>
```
La partie `<head>` contient les métadonnés. Le charset UTF-8 demande au navigateur d'interprèter les caractères spéciaux. Le `<title>` pour référencer le titre de la page. La balise `<link />` fait appel à la feuille de style qui gère le css.

Dans la partie `<body>` il y aura tout les élements visible de la page. 

### balise html
 
  ![](https://4.bp.blogspot.com/-B5vUzJXNAoE/Vuay2ygsN2I/AAAAAAAAG5o/-qOAVBa3LRkJ0fPWywYzkAcmezRAY2Rxg/s640/html-syntax.png)

###  Balises de structuration du texte

`<abbr>` : Abréviation  
`<blockquote>` : Citation (longue)  
`<cite>` : Citation du titre d’une œuvre ou d’un évènement  
`<q>` : Citation (courte)  
`<sup>` : Exposant  
`<sub>` : Indice  
`<strong>` : Mise en valeur forte  
`<em>` : Mise en valeur normale  
`<mark>` : Mise en valeur visuelle  
`<h1>` : Titre de niveau 1  
`<h2>` : Titre de niveau 2  
`<h3>` : Titre de niveau 3  
`<h4>` : Titre de niveau 4  
`<h5>` : Titre de niveau 5  
`<h6>` : Titre de niveau 6  
`<img />` : Image  
`<figure>` : Figure (image, code, etc.)  
`<figcaption>` : Description de la figure  
`<audio>` : Son  
`<video>` : Vidéo  
`<source>` : Format source pour les balises`<audio>`et`<video>`  
`<a>` : Lien hypertexte  
`<br />` : Retour à la ligne  
`<p>` : Paragraphe  
`<hr />` : Ligne de séparation horizontale  
`<address>` : Adresse de contact  
`<del>` : Texte supprimé  
`<ins>` : Texte inséré  
`<dfn>` : Définition  
`<kbd>` : Saisie clavier  
`<pre>` : Affichage formaté (pour les codes sources)  
`<progress>` : Barre de progression  
`<time>` : Date ou heure 

### Catégoriser dans un paragraphe  
Il y a en HTML des balises pour catégoriser des mots ou phrases à l'intérieur d'un paragraphe.  
Exemples :  

```
<p>Voici un <b>mots</b> en bold.</p>
<p>Voici un <em>mots</em> en italic.</p>
<p>Voici un <del>mots</del> supprimé.</p>
<p>Voici un <u>mots</u> souligné.</p>
```
<p>Voici un <b>mots</b> en bold.</p>
<p>Voici un <em>mots</em> en italic.</p>
<p>Voici un <del>mots</del> supprimé.</p>
<p>Voici un <u>mots</u> souligné.</p>
 
### Sélecteur de type id
Hors, il arrive que plusieurs éléments partageant la même balise doivent être mis en forme différemment. Une première façon de pointer vers un élément précis, unique dans la page, et de lui attribuer sa propre mise en forme, est de nommer cet élément dans la page html à l’aide de l’attribut html "id" :  

```
<div id="bloc">Mon contenu</div>
```  

et de pointer vers cet élément précis dans le css à l’aide du sélecteur `#bloc`
la règle css deviendra donc :  

```
#bloc{
	background:red;
}
```  

Cette manière de faire nécessite que l’élément html à mettre en forme ait un identifiant unique, spécifié dans le code html (ici "bloc").  

### Sélecteur de type class  

Pour sélectionner un ensemble plus précis que tous les éléments partageant une balise (exemple : le sélecteur div), et plus large qu’un élément unique (exemple : le sélecteur #bloc), il faut utiliser un type de sélecteur différent, un sélecteur de classe. Ce type de sélecteur nécessite de préciser dans l’html que les éléments concernés font partie d’un même ensemble. Pour ce faire, il faut utiliser l’attribut html "class" :

```
<div class="ensemble">Mon contenu 1</div>
<div class="ensemble">Mon contenu 2</div>
<div>Mon contenu 3</div>
```
et de pointer vers ces éléments à l’aide du sélecteur `.ensemble` la règle css deviendra donc:  
```
.ensemble{
   background: red;
}
``` 
La règle ci-dessus spécifie donc que les éléments html qui ont un attribut "class" de valeur "ensemble" doivent avoir un fond rouge. Le troisième élément `<div>` de l’exemple n’est pas concerné par la règle puisqu’il n’a pas la classe "ensemble".
La différence entre le type de sélecteur id (qui se spécifie dans le css avec le caractère #) et le type de sélecteur class (qui se spécifie dans le css avec le caractère .) est donc que le premier ne concerne qu’un élément et le second concerne un ensemble d’éléments.

