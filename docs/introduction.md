---
title: introduction
slug: intro
---

Il n’est pas question ici de vouloir réaliser des prouesses techniques ni de devenir developpeu·r·se, mais bien de se donner le droit d’ouvrir, sans rougir, toutes les boites qui nous intriguent. Opérer des transgressions dans les usages. Développer des méthodes combinatoires entre différents objets et programmes. Ouvrir les formats numériques. Essayer d’avoir un regard critique sur les choix effectués par les personnes, les communautés, les industriels, les consortiums qui façonnent nos environnements numériques.  
De cultiver ce qui est précieux dans tout dialogue : l’indéterminé et la surprise. Avancer par tâtonnements. Assumer nos incertitudes. Expérimenter avec les machines numériques afin d’accroître notre conscience et notre culture technique.

Le web, a comme essence des formats et langages ouverts. Ces derniers offrent une interopérabilité forte entre programmes et systèmes ce qui donne la possibilité de
concevoir des situations de jeux infinie. Parce qu’ils sont ouverts ces langages procure un pouvoir considérable d’analyse et d’inspection, l’objet web est par nature porteur de charge pédagogique. Mais aussi le web, peut-être considérer comme un bien commun, l’endroit important par lequel se passe le social, la politique et l’économique, pratiquer et concevoir le web augmente notre capacité à agir sur le monde.

---- 
Presentation [ histoire du hacking, d'internet et du Web](/slide_hacking/)
<iframe src="/slide_hacking" frameborder="0"></iframe>
