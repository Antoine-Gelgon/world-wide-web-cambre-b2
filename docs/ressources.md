---
title: ressources
slug: ressources
---

### Fonts libres de droits

* Use and Modify → [https://usemodify.com/](https://usemodify.com/)
* GoogleFonts →[https://fonts.google.com/](https://fonts.google.com/)
* Velvetyne → [https://www.velvetyne.fr/](https://www.velvetyne.fr/ )
* Font Squirrel → [https://www.fontsquirrel.com/](https://www.fontsquirrel.com/)
* Font Library → [https://fontlibrary.org/en]( https://fontlibrary.org/en)
* By Womxn → [http://design-research.be/by-womxn/](http://design-research.be/by-womxn/)
* OSP Foundry → [http://osp.kitchen/foundry/](http://osp.kitchen/foundry/)
* Sébastien San Filipo → [http://love-letters.be/foundry.html](http://love-letters.be/foundry.html)
* Omnibus Type → [https://www.omnibus-type.com/](https://www.omnibus-type.com/)
* Temporary State → [http://typefaces.temporarystate.net/preview/](http://typefaces.temporarystate.net/preview/)
* Classique free font -> [https://gitlab.com/Antoine-Gelgon/classic-fonts-libre](https://gitlab.com/Antoine-Gelgon/classic-fonts-libre)

