---
title: Hacking
type: slide
template: slide.html
slides:
 -
  url: /images/00-histoire-hack/09.png 
  caption: "1815 - *Ada Lovelace* -  À réaliser le premier véritable programme informatique, lors de son travail sur un ancêtre de l'ordinateur : la machine analytique de Charles Babbage" 
 -
  url: /images/00-histoire-hack/01.jpeg 
  caption: 1946 - Le *Tech Model Railroad Club (TMRC)* est un club de modélisme ferroviaire des étudiants du Massachusetts Institute of Technology 
 -
  url: /images/00-histoire-hack/02.jpg 
  caption: 1956 - TX-0, pour Transistorized Experimental computer zero 
 -
  url: https://upload.wikimedia.org/wikipedia/commons/f/f3/Punched_card.jpg
  caption: carte perforée
 -
  url: https://upload.wikimedia.org/wikipedia/commons/8/87/IBM_card_storage.NARA.jpg
  caption: cartes perforées - 1959 - 4 GO
 -
  url: /images/00-histoire-hack/07.png
  caption: L'Éthique hacker
 -
  url: /images/00-histoire-hack/03.jpg
  caption: Minskytron - premier programme graphique
 -
  url: /images/00-histoire-hack/06.jpg
  caption: Jude Milhon - Mère du cyberpunk
 -
  url: /images/00-histoire-hack/05.jpg
  caption: Margaret Hamilton - 1969 - Programme Apollo 
 -
  url: https://www.nybooks.com/wp-content/uploads/2023/05/letters_1-062223.jpg 
  caption: A. Michael Noll - 1960 - pionnier de l'art numérique 
 -
  url: https://www.fredzone.org/wp-content/uploads/2011/01/arpanet-1972-544x329.jpg
  caption: ARPANET -  1966
 -
  url: https://media.router-switch.com/media/wysiwyg/Help-Center-FAQ/Router/client-server-web.jpg
  caption: Bob Kahn - 1973 - TCP/IP  
 - 
  url: http://quentinm.canalblog.com/hypertexte.gif
  caption: Tim Berners-Lee & Robert Cailliau - 1989 - Invention du World Wide Web
---
