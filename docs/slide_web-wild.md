---
title: webWild 
type: slide
template: slide.html
slides:
 -
  title: 'WorldWildWeb !!'
  url:  ' ' 
  caption: " " 
 -
  title: 'Première page web -> http://info.cern.ch/hypertext/WWW/TheProject.html'
 -
  text: '"imaginé comme une plateforme ouverte qui permettrait à quiconque, partout, de partager des informations, de collaborer par-delà les frontières géographiques et culturelles." <br> -- Tim Berners-Lee - 1990'
 -
  text: '"Nous pouvons présumer un manque d’action de la part de l’utilisateur des réseaux sociaux les plus utilisés. Ce qui limite l’action d’un utilisateur, c’est-à-dire sa capacité à arrêter d’utiliser de telles plateformes, est un mélange de techniques addictives et de pressions sociales. "<br> -- Silvio Lorusso - Liquider l’utilisateur - 2022' 
 -
  title: Gilles Boccon-Gibod - http://perdu.com/ - 1998
 - 
  title: Web Revival
  text: 'https://thoughts.melonking.net/guides/introduction-to-the-web-revival-1-what-is-the-web-revival'
 - 
  title: Net.art
  text: Olia Lialina, Heath Bunting, Vuk Cosic, Jodi et Alexei Shulgin 
 -
  title: JODI - http://asdfg.jodi.org/-------------------------------/-------------------------------/-------------------------------/-----------------------102kd452/01lllb-01l1bb.html
 - 
  title: Digital Folklore - To computer users, with love and respect - Olia Lialina 
  url: https://manuelbuerger.com/static/85e54637d14317eab9e349a2daa56a45/digitalfolklore-cover.gif
 - 
  title: Cascade -  Olga Kisseleva - 1999
  text: http://synesthesie.xyz/html/cascade
 - 
  title: Timothé Rolin - http://www.adamproject.net/ - 2001 
 -
  title: Jodi - https://geogoo.net/
 - 
  title: Martin  Kleep - aem1k.com
 - 
  title: Jonathan Puckey - pointerpointer.com
 -
  title: Studio Moniker - https://studiomoniker.com/projects/puff-up-club
 -
  title: Studio Moniker - Kilo by Light Light - Do Not Touch - https://www.youtube.com/watch?v=W3ZjY8YD_NY
 -
  title: Rafaël Rozendaal - https://www.newrafael.com/websites/
 - 
  title: Raphaël Bastide - https://fungal.page/
 -
  title: Raphaël Bastide - https://raphaelbastide.com/1962/ 
 -
  title: https://archive.org/
  
---
